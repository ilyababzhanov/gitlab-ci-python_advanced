from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from typing import List
from datetime import datetime

db = SQLAlchemy()


def create_app():
    from .models import Client, Parking, ClientParking

    app = Flask(__name__)
    app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///database.db'
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)

    with app.app_context():
        db.create_all()

    @app.route("/clients", methods=['GET'])
    def get_clients_handler():
        """Список всех клиентов"""
        clients: List[Client] = db.session.query(Client).all()
        clients_list = [c.to_json() for c in clients]
        return jsonify(clients_list), 200

    @app.route("/clients/<int:client_id>", methods=['GET'])
    def get_client_handler(client_id: int):
        """Информация о клиенте по ID"""
        client: Client = db.session.query(Client).get(client_id)
        return jsonify(client.to_json()), 200

    @app.route("/clients", methods=['POST'])
    def create_client_handler():
        """Создание нового клиента"""
        name = request.form.get('name', type=str)
        surname = request.form.get('surname', type=str)
        credit_card = request.form.get('credit_card', type=str)
        car_number = request.form.get('car_number', type=str)

        new_client = Client(name=name,
                            surname=surname,
                            credit_card=credit_card,
                            car_number=car_number)

        db.session.add(new_client)
        db.session.commit()
        return '', 201

    @app.route("/parkings", methods=['POST'])
    def create_parking_handler():
        """Создание новой парковочной зоны"""
        address = request.form.get('address', type=str)
        opened = request.form.get('opened', type=bool)
        count_places = request.form.get('count_places', type=int)
        count_available_places = request.form.get('count_available_places', type=int)

        new_parking = Parking(address=address,
                              opened=opened,
                              count_places=count_places,
                              count_available_places=count_available_places)

        db.session.add(new_parking)
        db.session.commit()
        return '', 201

    @app.route("/client_parkings", methods=['POST'])
    def check_in_handler():
        """Заезд на парковку"""
        client_id = request.form.get('client_id', type=int)
        parking_id = request.form.get('parking_id', type=int)

        # Проверка наличия свободных мест
        parking: Parking = db.session.query(Parking).get(parking_id)
        if parking.count_available_places <= 0:
            return 'No available parking spaces', 400

        # Фиксация даты заезда
        time_in = datetime.now()

        # Уменьшение количества свободных мест
        parking.count_available_places -= 1

        # Создание записи о въезде на парковку
        new_entry = ClientParking(client_id=client_id,
                                  parking_id=parking_id,
                                  time_in=time_in)

        db.session.add(new_entry)
        db.session.commit()
        return '', 201

    @app.route("/client_parkings", methods=['DELETE'])
    def check_out_handler():
        """Выезд с парковки"""
        client_id = request.form.get('client_id', type=int)
        parking_id = request.form.get('parking_id', type=int)

        # Проставление времени выезда
        time_out = datetime.now()

        # Увеличение количества свободных мест
        parking: Parking = db.session.query(Parking).get(parking_id)
        parking.count_available_places += 1

        # Обновление записи о въезде-выезде
        entry: ClientParking = db.session.query(ClientParking).filter_by(client_id=client_id,
                                                                         parking_id=parking_id).first()
        entry.time_out = time_out

        db.session.commit()
        return '', 200

    return app
