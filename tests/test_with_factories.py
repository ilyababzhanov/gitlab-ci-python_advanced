import pytest
from module_29_testing.hw.flask_testing.tests.factories import ClientFactory, ParkingFactory
from module_29_testing.hw.flask_testing.main.models import Client, Parking


@pytest.mark.parametrize("endpoint", ["/clients", "/parkings"])
def test_create_client_with_factory(client, db, app, endpoint):
    # Используем ClientFactory для создания тестового клиента
    client_data = ClientFactory()
    response = client.post(endpoint, data=client_data.__dict__)

    assert response.status_code == 201

    # Проверяем, что клиент успешно создан в базе данных
    created_client = db.session.query(Client).filter_by(name=client_data.name).first()
    assert created_client is not None


def test_create_parking_with_factory(client, db, app):
    # Используем ParkingFactory для создания тестовой парковки
    parking_data = ParkingFactory()
    response = client.post("/parkings", data=parking_data.__dict__)

    assert response.status_code == 201

    # Проверяем, что парковка успешно создана в базе данных
    created_parking = db.session.query(Parking).filter_by(address=parking_data.address).first()
    assert created_parking is not None
