import pytest
from module_29_testing.hw.flask_testing.main.routes import create_app, db as _db
from module_29_testing.hw.flask_testing.main.models import Client, Parking, ClientParking
from datetime import datetime


@pytest.fixture
def app():
    _app = create_app()
    _app.config["TESTING"] = True
    _app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"

    with _app.app_context():
        _db.create_all()

        yield _app

        _db.session.remove()
        _db.drop_all()


@pytest.fixture
def client(app):
    client = app.test_client()
    yield client


@pytest.fixture
def db(app):
    with app.app_context():
        _db.create_all()
        yield _db
        _db.session.remove()


@pytest.mark.parametrize("endpoint", ["/clients", "/parkings", "/clients/1", "/client_parkings"])
def test_get_endpoints_return_200(client, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 200


def test_create_client(client, db):
    data = {
        "name": "NewClient",
        "surname": "NewSurname",
        "credit_card": "1111222233334444",
        "car_number": "XYZ789"
    }
    response = client.post("/clients", data=data)
    assert response.status_code == 201

    created_client = db.session.query(Client).filter_by(name="NewClient").first()
    assert created_client is not None


def test_create_parking(client, db):
    data = {
        "address": "NewParking",
        "opened": True,
        "count_places": 5,
        "count_available_places": 5
    }
    response = client.post("/parkings", data=data)
    assert response.status_code == 201

    created_parking = db.session.query(Parking).filter_by(address="NewParking").first()
    assert created_parking is not None


@pytest.mark.parking
def test_check_in(client, db):
    data = {
        "client_id": 1,
        "parking_id": 1
    }
    response = client.post("/client_parkings", data=data)
    assert response.status_code == 201

    entry = db.session.query(ClientParking).filter_by(client_id=1, parking_id=1).first()
    assert entry is not None
    assert entry.time_in is not None


def test_check_out(client, db):
    # Создаем запись для выезда
    entry = ClientParking(client_id=1, parking_id=1, time_in=datetime.now())
    db.session.add(entry)
    db.session.commit()

    data = {
        "client_id": 1,
        "parking_id": 1
    }
    response = client.delete("/client_parkings", data=data)
    assert response.status_code == 200

    entry = db.session.query(ClientParking).filter_by(client_id=1, parking_id=1).first()
    assert entry is not None
    assert entry.time_out is not None
    assert entry.time_out >= entry.time_in


@pytest.mark.smoke
def test_smoke_get_endpoints_return_200(client, endpoint):
    response = client.get(endpoint)
    assert response.status_code == 200